Python, flask server dependencies.
```commandline
$ docker image build -t nathan5280/flask-server:v0 -f Dockerfile-flask-server .
$ docker push nathan5280/flask-server:v0
```

Calculator Server
```commandline
$ docker image build -t nathan5280/calculator:v0 -f Dockerfile-calculator .
$ docker push nathan5280/calculator:v0
```

Run Server
```commandline
$ docker container run --rm --name calculator nathan5280/calculator:v0
```

Run Cluster
```commandline
$ docker-compose --file calculator.yml up -d
```