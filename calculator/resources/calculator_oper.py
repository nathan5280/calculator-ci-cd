from flask_restful import Resource, reqparse

from models import RPNCalculator, InvalidContextError, OperandError


class CalculatorOper(Resource):
    """Resource wrapper to create a new Calculator"""
    parser = reqparse.RequestParser()

    parser.add_argument('operand',
                        type=float,
                        required=False)

    calculator = RPNCalculator()

    operator_functions = {
        'add': calculator.add,
        'sub': calculator.sub,
        'mul': calculator.mul,
        'div': calculator.div,
        'neg': calculator.neg
    }

    @classmethod
    def put(cls, calc_id: int, operator: str):
        if 'push' == operator:
            data = cls.parser.parse_args()
            return {'result': cls.calculator.push(id_=calc_id, operand=data['operand'])}, 200

        operator_function = cls.operator_functions.get(operator, None)
        if operator_function:
            return {'result': operator_function(id_=calc_id)}, 200

        return {'message': f"Operation: {operator} not supported."}, 404
