from flask_restful import Resource, reqparse

from models import RPNCalculator, InvalidContextError, OperandError


class CalculatorStack(Resource):
    calculator = RPNCalculator()

    @classmethod
    def get(cls, calc_id: int):
        """Get the last result."""
        try:
            result = cls.calculator.result(id_=calc_id)
        except InvalidContextError:
            return {'message': "Invalid context."}, 400
        except OperandError:
            return {'message': "No result available."}, 400

        return {'result': result}

    @classmethod
    def delete(cls, calc_id: int):
        """Remove the Calculator"""
        cls.calculator.delete(id_=calc_id)
