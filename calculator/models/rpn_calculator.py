from typing import Callable

# from models.stack import Stack
from models.db_stack import Stack as Stack
from models import OperandError


class RPNCalculator:
    """Simple RPN calculator built around an in memory stack."""
    # stack = None
    stack = Stack()

    @classmethod
    def _binary_op(cls, *, operator: Callable, id_: int) -> float:
        """Apply the operation to the last two numbers on the stack."""
        try:
            x_operand = cls.stack.pop(id_=id_)
        except IndexError:
            raise OperandError("Stack empty.  Missing first operand.")

        try:
            y_operand = cls.stack.pop(id_=id_)
        except IndexError:
            raise OperandError("Stack empty.  Missing second operand.")

        result = operator(x_operand, y_operand)
        pushed_result = cls.stack.push(operand=result, id_=id_)
        return pushed_result

    @classmethod
    def _urnary_op(cls, *, operator: Callable, id_: int) -> float:
        """Apply the operation to the last number on the stack."""
        try:
            x_operand = cls.stack.pop(id_=id_)
        except IndexError:
            raise OperandError("Stack empty.  Missing first operand.")

        result = operator(x_operand)
        pushed_result = cls.stack.push(operand=result, id_=id_)
        return pushed_result

    @classmethod
    def start(cls):
        """Start a new calculation with a fresh stack."""
        # return cls.stack.id_
        return cls.stack.start()

    @classmethod
    def push(cls, operand: float, id_: int) -> float:
        """Push a number onto the stack."""
        pushed_operand = cls.stack.push(operand=operand, id_=id_)
        return pushed_operand

    @classmethod
    def add(cls, id_: int) -> float:
        """Add the last two numbers on the stack."""
        result = cls._binary_op(operator=lambda x, y: y + x, id_=id_)
        return result

    @classmethod
    def sub(cls, id_: int) -> float:
        """Subtract the last two numbers on the stack."""
        result = cls._binary_op(operator=lambda x, y: y - x, id_=id_)
        return result

    @classmethod
    def mul(cls, id_: int) -> float:
        """Multiply the last two numbers on the stack."""
        result = cls._binary_op(operator=lambda x, y: y * x, id_=id_)
        return result

    @classmethod
    def div(cls, id_: int) -> float:
        """Divide the last two numbers on the stack."""
        result = cls._binary_op(operator=lambda x, y: y / x, id_=id_)
        return result

    @classmethod
    def neg(cls, id_: int) -> float:
        """Negate the last number on the stack."""
        result = cls._urnary_op(operator=lambda x: -x, id_=id_)
        return result

    @classmethod
    def result(cls, id_: int) -> float:
        """Get the last entry in the stack."""
        try:
            result = cls.stack.peek(id_=id_)
            return result
        except IndexError:
            raise OperandError("Stack empty.")

    @classmethod
    def delete(cls, id_: int) -> None:
        """Delete the calculation."""
        # cls.start()
        cls.stack.delete(id_=id_)
