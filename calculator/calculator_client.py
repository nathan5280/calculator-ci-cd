"""Client to invoke the add function in the Calculator Server through the RESTful API"""

import sys
import requests


def test_add(ip_: str, x: float, y: float) -> float:
    # given
    url = f'http://{ip_}:5000/calculator/v0'
    response = requests.post(url)
    response_data = response.json()
    calc_url = response_data['location']

    requests.put(f"http://{ip_}:5000{calc_url}/push", json={'operand': y})
    requests.put(f"http://{ip_}:5000{calc_url}/push", json={'operand': x})
    response = requests.put(f"http://{ip_}:5000{calc_url}/add")

    response_data = response.json()

    requests.delete(f"http://{ip_}:5000{calc_url}")

    return response_data['result']


if __name__ == '__main__':
    ip = sys.argv[1]
    result = test_add(ip, 1, 2)
    print(f'result={result}')
