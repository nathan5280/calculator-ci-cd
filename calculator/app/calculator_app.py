from time import sleep

from flask import Flask
from flask_restful import Api

from resources import Calculator, CalculatorOper, CalculatorStack
from models.db import create_db

from sqlalchemy.exc import OperationalError

DB_SERVER_NAME = "calculator-ci-cd_db_1"


class CalculatorApp:
    """RESTful API server for the calculator."""

    def __init__(self):
        self.app = Flask(__name__)

        print("Connecting ...")
        for _ in range(5):
            try:
                create_db(connection_string=f'postgresql://postgres:example@{DB_SERVER_NAME}/postgres')
                break
            except OperationalError:
                print("Waiting for DB")
                sleep(1)

        print("Connected ...")

        self._api = Api(self.app)

        # Expose the calculator endpoint.
        # Request a new calculator be created.
        self._api.add_resource(Calculator, '/calculator/v0')

        # Access the operations on the calculator based on the calculator location url return when
        # the calculator was requested.  (push, add, sub, result, delete)
        self._api.add_resource(CalculatorOper, '/calculator/v0/<int:calc_id>/<string:operator>')

        # Operate on the stack.
        self._api.add_resource(CalculatorStack, '/calculator/v0/<int:calc_id>')
