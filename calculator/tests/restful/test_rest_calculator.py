from flask.testing import FlaskClient


def test_add_pass(client: FlaskClient, calc_url: str) -> None:
    # given
    y, x = 1, 2

    # when (add)
    client.put(f"{calc_url}/push", json={'operand': y})
    client.put(f"{calc_url}/push", json={'operand': x})
    client.put(f"{calc_url}/add")

    # then
    response = client.get(calc_url)
    assert 200 == response.status_code
    response_data = response.get_json()
    assert x + y == response_data['result']


def test_sub_pass(client: FlaskClient, calc_url: str) -> None:
    # given
    y, x = 2, 1

    # when (sub)
    client.put(f"{calc_url}/push", json={'operand': y})
    client.put(f"{calc_url}/push", json={'operand': x})
    client.put(f"{calc_url}/sub")

    # then
    response = client.get(calc_url)
    assert 200 == response.status_code
    response_data = response.get_json()
    assert y - x == response_data['result']


def test_mul_pass(client: FlaskClient, calc_url: str) -> None:
    # given
    y, x = 2, 1

    # when (sub)
    client.put(f"{calc_url}/push", json={'operand': y})
    client.put(f"{calc_url}/push", json={'operand': x})
    client.put(f"{calc_url}/mul")

    # then
    response = client.get(calc_url)
    assert 200 == response.status_code
    response_data = response.get_json()
    assert y * x == response_data['result']


def test_div_pass(client: FlaskClient, calc_url: str) -> None:
    # given
    y, x = 2, 1

    # when (sub)
    client.put(f"{calc_url}/push", json={'operand': y})
    client.put(f"{calc_url}/push", json={'operand': x})
    client.put(f"{calc_url}/div")

    # then
    response = client.get(calc_url)
    assert 200 == response.status_code
    response_data = response.get_json()
    assert y / x == response_data['result']


def test_neg_pass(client: FlaskClient, calc_url: str) -> None:
    # given
    x = 1

    # when (sub)
    client.put(f"{calc_url}/push", json={'operand': x})
    client.put(f"{calc_url}/neg")

    # then
    response = client.get(calc_url)
    assert 200 == response.status_code
    response_data = response.get_json()
    assert -x == response_data['result']


def test_close_pass(client: FlaskClient, calc_url: str) -> None:
    # given

    # when (close)
    client.delete(calc_url)

    # then (No calculator at the url)
    response = client.get(calc_url)
    assert 400 == response.status_code


def test_bad_operator_fail(client: FlaskClient, calc_url: str) -> None:
    # given

    # when (bad operator)
    response = client.put(f"{calc_url}/bad_oper")

    # then
    assert 404 == response.status_code


def test_result_bad_context_fail(client: FlaskClient, calc_url: str) -> None:
    # given
    calc_url = calc_url + "0"

    # when (result, bad context)
    response = client.get(calc_url)

    # then (No calculator at the url)
    assert 400 == response.status_code


def test_result_no_operand_fail(client: FlaskClient, calc_url: str) -> None:
    # given

    # when (result, no operand)
    response = client.get(calc_url)

    # then
    assert 400 == response.status_code
