import pytest
from flask.testing import FlaskClient

from app import CalculatorApp


@pytest.fixture
def client() -> FlaskClient:
    """Get a test client for the AddCalculator RESTful API server."""
    calculator = CalculatorApp()
    return calculator.app.test_client()


@pytest.fixture
def calc_url(client: FlaskClient):
    """Request a new calculator be created."""
    url = '/calculator/v0'
    response = client.post(url)
    assert 201 == response.status_code
    response_data = response.get_json()
    stack_url = response_data['location']

    yield stack_url

    client.delete(stack_url)

# Note that PyTest is smart enough to only call the client test fixture once for each test
# even though the test takes it as an argument and the cacl_url takes it as an argument.
