from app import CalculatorApp

print("Creating Calculator Application")
app = CalculatorApp()
print("Starting Calculator Server")
app.app.run(host='0.0.0.0', port=5000, debug=True)
print("Stopping Calculator Server")
